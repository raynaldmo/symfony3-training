<?php


namespace AppBundle\Twig;

use AppBundle\Service\MarkdownTransformer;

/**
 * Class MarkdownExtension
 *
 * @package AppBundle\Twig
 */
class MarkdownExtension extends \Twig_Extension
{

    /**
     * @var \AppBundle\Service\MarkdownTransformer
     */
    private $markdownTransformer;

    /**
     * MarkdownExtension constructor.
     *
     * @param \AppBundle\Service\MarkdownTransformer $markdownTransformer
     */
    public function __construct(MarkdownTransformer $markdownTransformer)
    {
        $this->markdownTransformer = $markdownTransformer;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_markdown';
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
          new \Twig_SimpleFilter(
              'markdownify',
              [$this, 'parseMarkdown'],
              ['is_safe' => ['html']]
          ),
        ];
    }

    /**
     * @param $str
     *
     * @return string
     */
    public function parseMarkdown($str)
    {
        return $this->markdownTransformer->parse($str);
    }
}
