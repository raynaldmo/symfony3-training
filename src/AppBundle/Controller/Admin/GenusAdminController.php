<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Genus;
use AppBundle\Form\GenusFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_EDITOR')")
 */
class GenusAdminController extends Controller
{

    /**
     * @Route("/genus", name="admin_genus_list")
     */
    public function indexAction()
    {
        $genuses = $this->getDoctrine()
          ->getRepository('AppBundle:Genus')
          ->findAll();

        return $this->render(
            'admin/genus/list.html.twig',
            [
            'genuses' => $genuses,
          ]
        );
    }

    /**
     * @Route("/genus/new", name="admin_genus_new")
     *
     * @param $request
     * @return \Symfony\Component\HttpFoundation\Response $response
     */
    public function newAction(Request $request)
    {
        // This also works
        // $form = $this->createForm('\AppBundle\Form\GenusFormType');

        // Use shortcut for fully qualified class name
        $form = $this->createForm(GenusFormType::class);

        // only handles data on POST
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // dump($form->getData()); die;

            // We get a complete genus object by virtue of the code in
            // GenusFormType:configureOptions()
            $genus = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($genus);
            $em->flush();

            /** @var \AppBundle\Entity\User $user */
            $user = $this->getUser();
            $email = $user->getEmail();
            // dump($user); dump($email); die();

            $this->addFlash('success',
              sprintf('Genus %s created by %s',
                $genus->getName(), $email));

            return $this->redirectToRoute('admin_genus_list');
        }


        $rendered_form = $this->render(
            'admin/genus/new.html.twig',
            [
            'genusForm' => $form->createView(),
          ]
        );

        return $rendered_form;
    }

    /**
     * @Route("/genus/{id}/edit", name="admin_genus_new_edit")
     *
     * @param $request
     * @return \Symfony\Component\HttpFoundation\Response $response
     */
    public function editAction(Request $request, Genus $genus)
    {

        // This also works
        // $form = $this->createForm('\AppBundle\Form\GenusFormType');

        // Use shortcut for fully qualified class name
        $form = $this->createForm(GenusFormType::class, $genus);

        // only handles data on POST
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // dump($form->getData()); die;

            // We get a complete genus object by virtue of the code in
            // GenusFormType:configureOptions()
            $genus = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($genus);
            $em->flush();

            $this->addFlash('success', 'Genus '.$genus->getName().' updated.');

            return $this->redirectToRoute(
                'genus_show',
                ['genusId' => $genus->getId()]
            );
        }


        $rendered_form = $this->render(
            'admin/genus/new.html.twig',
            [
            'genusForm' => $form->createView(),
          ]
        );

        return $rendered_form;
    }
}
