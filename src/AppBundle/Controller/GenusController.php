<?php
/**
 * Created by PhpStorm.
 * User: raynald
 * Date: 9/3/19
 * Time: 5:08 PM
 */

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Genus;
use AppBundle\Entity\GenusNote;
use AppBundle\Service\MarkdownTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class GenusController extends Controller
{

    /**
     * @Route("/genus/new")
     */
    public function newAction()
    {

        return new Response(
          "
            <html>
              <body>
              Go to <a href='/admin/genus'>Create Genus</a>&nbsp;to create a Genus 
              </body>
            </html>
          "
        );
/*
        // The correct way to add a new "Genus" would be via a form
        // For now though we'll hard-code the values
        $genus = new Genus();
        $name = 'Octopus';
        $genus->setName($name);
        $genus->setSpeciesCount(rand(100, 100000));
        $genus->setFunFact('_I am the wonderful '.$genus->getName().'_');
        $genus->setSubFamily('Octopodinae');


        $note = new GenusNote();
        $note->setUsername('AquaWeaver');
        $note->setUserAvatarFilename('ryan.jpeg');
        $note->setNote('I counted 8 legs... as they wrapped around me');
        $note->setCreatedAt(new \DateTime('-1 month'));

        // Save genus note object
        // A relationship in DB table will automatically be created by Doctrine
        // since we added genus object and ManyToOne annotation in GenusNote class
        $note->setGenus($genus);

        $em = $this->getDoctrine()->getManager();

        // Can persist in any order
        $em->persist($genus);
        $em->persist($note);
        $em->flush();

        return new Response("<html><body>Genus $name created.</body></html>");
 */
    }

    /**
     * @Route("/genus", name="genus_list")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        // This uses the explicit class name
        // $genuses = $em->getRepository('AppBundle\Entity\Genus');

        // Load up the entire table
        // Use special syntax for class
        // $repo was an EntityRepository object but became a
        // GenusRepository object
        $repo = $em->getRepository('AppBundle:Genus');

        // dump($repo);

        $genuses = $repo->findAllPublishedOrderedByRecentlyActive();

        // $genuses is an array of objects
        // dump($genuses); die;

        return $this->render(
            'genus/list.html.twig',
            [
            'genuses' => $genuses,
          ]
        );
    }

    /**
     * @Route("/genus/{genusId}", name="genus_show")
     */
    public function showAction($genusId)
    {

        // This is the controller for the "/genus" route
        // The name of the controller is arbitrary
        // The controller must return a Response object

        // Get Entity Manager
        $em = $this->getDoctrine()->getManager();
        /** @var Genus $genus */
        $genus = $em->getRepository('AppBundle:Genus')
          ->findOneBy(['id' => $genusId]);

        if (!$genus) {
            throw $this->createNotFoundException('No genus found!');
        }

        $this->get('logger')->info('Showing: '.$genus->getName());

        /*
        // Remember that getNotes() returns an ArrayCollection
        // This is an in-efficient way to do this though as it loads up all
        // notes before filtering
        $recentNotes = $genus->getNotes()
            ->filter(function (GenusNote $note) {
              return $note->getCreatedAt() > new \DateTime('-3 months');
            });

        */

        $repo = $em->getRepository('AppBundle:GenusNote');
        $recentNotes = $repo->findAllRecentNotesForGenus($genus);
        // var_dump($recentNotes);die();

        /*
        // Example of how to use caching and markdown service
        ///
        // All this got moved into app.markdown_transformer service
        $cache = $this->get('doctrine_cache.providers.my_markdown_cache');
        $funFact = $genus->getFunFact();
        $key = md5($funFact);

        if ($cache->contains($key)) {
          $funFact = $cache->fetch($key);
        } else {
          sleep(0);
          $funFact = $this->get('markdown.parser')
            ->transform($genus->getFunFact());
          $cache->save($key, $funFact);
        }

        $genus->setFunFact($funFact);
    */
        // We don't use this service and instead rely on 'markdownify' Twig Filter
        // to perform the markdown to HTML conversion
        // Get our custom service from service container
        // $markdownParser = $this->get('app.markdown_transformer');
        // $funFact = $markdownParser->parse($genus->getFunFact());

        return $this->render(
            'genus/show.html.twig',
            [
            'genus' => $genus,
              // 'funFact' => $funFact,
            'recentNoteCount' => count(($recentNotes)),
          ]
        );
    }


    /**
     * @Route("/genus/{genusName}/notes", name="genus_show_notes", methods={"GET"})
     *
     * @param $genusName
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getNotesAction($genusName)
    {
        // This method doesn't use param conversion

        // Get Entity Manager
        $em = $this->getDoctrine()->getManager();
        $genus = $em->getRepository('AppBundle:Genus')
          ->findBy(['name' => $genusName]);

        // dump($genus);

        $notes = $em->getRepository('AppBundle:GenusNote')
          ->findBy(['genus' => $genus]);

        // dump($notes);
        $genusNotes = [];
        foreach ($notes as $note) {
            $genusNotes[] = [
              'id' => $note->getId(),
              'username' => $note->getUsername(),
              'avatarUri' => '/images/'.$note->getUserAvatarFilename(),
              'note' => $note->getNote(),
              'date' => $note->getCreatedAt()->format('M d, Y'),
            ];
        }


        /*
         $notes = [
           ['id' => 1, 'username' => 'AquaPelham', 'avatarUri' => '/images/leanna.jpeg', 'note' => 'Octopus asked me a riddle, outsmarted me', 'date' => 'Dec. 10, 2015'],
           ['id' => 2, 'username' => 'AquaWeaver', 'avatarUri' => '/images/ryan.jpeg', 'note' => 'I counted 8 legs... as they wrapped around me', 'date' => 'Dec. 1, 2015'],
           ['id' => 3, 'username' => 'AquaPelham', 'avatarUri' => '/images/leanna.jpeg', 'note' => 'Inked!', 'date' => 'Aug. 20, 2015'],
         ];

       */

        $data = [
          'notes' => $genusNotes,
        ];

        // return new Response(json_encode($data));
        return new JsonResponse($data);
    }


    /**
     * @Route("/genus/{name}/notes", name="genus_show_notes_v2", methods={"GET"})
     *
     * @param \AppBundle\Entity\Genus $genus
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getNotesAction_v2(Genus $genus)
    {

        // "Param conversion" at work here. It allows us to pass in and have available
        // to us the genus Object
        // dump($genus);

        $notes = array();

        // $genus->getNotes() will trigger two queries
        // first to get genus object by name - due to having name in the route
        // second to get associated notes
        foreach ($genus->getNotes() as $note) {
            // dump($note);
            $notes[] = [
              'id' => $note->getId(),
              'username' => $note->getUsername(),
              'avatarUri' => '/images/'.$note->getUserAvatarFilename(),
              'note' => $note->getNote(),
              'date' => $note->getCreatedAt()->format('M d, Y'),
            ];
        }

        $data = [
          'notes' => $notes,
        ];

        // return new Response(json_encode($data));
        return new JsonResponse($data);
    }
}
