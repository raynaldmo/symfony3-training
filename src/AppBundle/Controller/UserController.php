<?php

namespace AppBundle\Controller;
use AppBundle\Form\UserRegistrationForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * @Route("/register", name="user_register")
     */
    public function registerAction( Request $request)
    {
        $form = $this->createForm(UserRegistrationForm::class);

        $form->handleRequest($request);
        // No need to add isSubmitted() as isValid() calls that
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \AppBundle\Entity\User $user */
            $user = $form->getData();
            $user->setRoles(['ROLE_USER']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Welcome '. $user->getEmail());

            // return $this->redirectToRoute('homepage');

            // Automatically log user in after he registers
            $guardHandler = $this->get('security.authentication.guard_handler');

            $response = $guardHandler->authenticateUserAndHandleSuccess(
              $user,
              $request,
              $this->get('app.security.login_form_authenticator'),
              'main'
            );

            return $response;
        }

        $view = $this->render('user/register.html.twig', [
            'form' => $form->createView()
          ]
        );

        return $view;

    }
}
