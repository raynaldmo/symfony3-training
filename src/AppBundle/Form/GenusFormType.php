<?php

namespace AppBundle\Form;

use AppBundle\Entity\SubFamily;
use AppBundle\Repository\SubFamilyRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Genus;

class GenusFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Form element names should match Genus class property names
        $builder->add('name')
          ->add(
              'subFamily',
              EntityType::class,
              [
              'class' => SubFamily::class,
              'placeholder' => 'Choose a Sub Family',
              'query_builder' => function (SubFamilyRepository $repo) {
                  return $repo->sortSubFamilySelection();
              },
            ]
          )
          ->add('speciesCount')
          ->add('funFact')
          ->add(
              'isPublished',
              ChoiceType::class,
              [
              'choices' => [
                'Yes' => true,
                'No' => false,
              ],
            ]
          )
          ->add(
              'firstDiscoveredAt',
              DateType::class,
              [
              'widget' => 'single_text',
              'attr' => [
                'class' => 'js-date-picker',
              ],
              'html5' => false,
              'label' => 'Discovered at',
            ]
          )
          ->add(
              'save',
              SubmitType::class,
              [
              'attr' => [
                'class' => 'btn btn-primary',
                'formnovalidate' => true,
              ],
            ]
          );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
            'data_class' => Genus::class,
          ]
        );
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_genus_form_type';
    }
}
