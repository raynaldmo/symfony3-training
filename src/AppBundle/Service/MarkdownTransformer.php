<?php

namespace AppBundle\Service;

use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;

use Doctrine\Common\Cache\Cache;

/**
 * Class MarkdownTransformer
 *
 * @package AppBundle\Service
 */
class MarkdownTransformer
{

    /**
     * @var
     */
    private $markdownParser;
    private $cache;

    /**
     * MarkdownTransformer constructor.
     *
     * @param \Knp\Bundle\MarkdownBundle\MarkdownParserInterface markdownParser
     * @param \Doctrine\Common\Cache\Cache $cache
     */
    public function __construct(
        MarkdownParserInterface $markdownParser,
        Cache $cache
    ) {
        $this->markdownParser = $markdownParser;
        $this->cache = $cache;
    }


    /**
     * @param string $str
     *
     * @return string
     */
    public function parse(string $str)
    {

        // NB: caching is only enabled for PROD environment
        // because of config_dev.yml cache_type: array
        $cache = $this->cache;
        $key = md5($str);

        if ($cache->contains($key)) {
            return $cache->fetch($key);
        }

        sleep(0);
        $str = $this->markdownParser->transformMarkdown($str);
        $cache->save($key, $str);

        return $str;
    }
}
