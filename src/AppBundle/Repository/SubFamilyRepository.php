<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SubFamilyRepository extends EntityRepository
{

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function sortSubFamilySelection()
    {
        return $this->createQueryBuilder('sub_family')
          ->orderBy('sub_family.name', 'ASC');
    }
}
