<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class GenusRepository extends EntityRepository
{

    /**
     * @return \AppBundle\Entity\Genus array
     */
    public function findAllPublishedOrderedBySize()
    {
        return $this->createQueryBuilder('genus')
          ->andWhere('genus.isPublished = :isPublished')
          ->setParameter('isPublished', true)
          ->orderBy('genus.speciesCount', 'DESC')
          ->getQuery()
          ->execute();
    }

    /**
     * @return \AppBundle\Entity\Genus array
     */
    public function findAllPublishedOrderedByName()
    {
        return $this->createQueryBuilder('genus')
          ->andWhere('genus.isPublished = :isPublished')
          ->setParameter('isPublished', true)
          ->orderBy('genus.name', 'ASC')
          ->getQuery()
          ->execute();
    }


    /**
     * @return \AppBundle\Entity\Genus array
     */
    public function findAllPublishedOrderedByRecentlyActive()
    {
        return $this->createQueryBuilder('genus')
          ->andWhere('genus.isPublished = :isPublished')
          ->setParameter('isPublished', true)
          ->leftJoin('genus.notes', 'genus_note')
          ->getQuery()
          ->execute();
    }
}
