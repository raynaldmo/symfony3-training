aqua_note
=========

A Symfony project created on August 25, 2019, 1:16 am.

### Intro
* SymfonyCasts "Joyful Development with Symfony 3" video course  
  * https://symfonycasts.com/screencast/symfony3

### Install Symfony Installer
```
raynald@Raynalds-iMac ~ $ pwd
/Users/raynald

raynald@Raynalds-iMac ~ $ sudo mkdir -p /usr/local/bin
raynald@Raynalds-iMac ~ $ sudo curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony
raynald@Raynalds-iMac ~ $ sudo chmod a+x /usr/local/bin/symfony
 ```
 
### Install Symfony
```
raynald@Raynalds-iMac ~/PhpStormProjects/symfony3-training $ pwd
/Users/raynald/PhpStormProjects/symfony3-training  
raynald@Raynalds-iMac ~/PhpStormProjects/symfony3-training $ symfony new symfony3-training
```

### Start Web server and Launch site
```
raynald@Raynalds-iMac ~/PhpStormProjects/symfony3-training $ pwd
/Users/raynald/PhpStormProjects/symfony3-training  

# Start built-in PHP web server
raynald@Raynalds-iMac ~/PhpStormProjects/symfony3-training $ php bin/console server:run 
```  
```
PHP 7.0.20 Development Server started at Tue Sep  3 09:35:26 2019
Listening on http://127.0.0.1:8000
Document root is /Users/raynald/PhpStormProjects/symfony3-training/web
Press Ctrl-C to quit.

```

### Symfony Console
* Use Symfony Console (CLI) to debug and show configuration, routes, services etc
```
raynald@Raynalds-iMac ~/PhpStormProjects/symfony3-training (master) $ pwd
/Users/raynald/PhpStormProjects/symfony3-training
raynald@Raynalds-iMac ~/PhpStormProjects/symfony3-training (master) $    

raynald@Raynalds-iMac ~/PhpStormProjects/symfony3-training (master) $ ./bin/console 
Symfony 3.4.30 (kernel: app, env: dev, debug: true)
...
```
### Start "Joyful Development with Symfony 3" course

### 03 First Page
#### Directory contents
* src/ - Holds all PHP files (classes)  
* app/ - Holds configuration, templates  
* Creating a page in Symfony involves two steps:
  * Define a route - **_configuration that says what the URL is_**
  * Define controller that handles the route - **_function that builds the page_**
* Create ```GenusController.php``` version 1

### 04 Routing Wildcards
* Example wildcard route: ```@Route("/genus/{genusName}")```  
* Use Symfony console for debug commands, showing configuration etc.  
```php bin/console```

### 05 Intro to Services
* Symfony comes with lots of objects that perform some functionality
  * Access database
  * Send mail
  * Log messages
  * Return JSON response
  * etc.
* These objects are called **_Services_**
* In Symfony **_all_** functionality is done by a service
* Symfony implements a **_Service Container_** to keep track of services
  * Each service is accessed via a key in the **_Service Container_**
* Views/templates reside in ```app/Resources/views```

### 06 Listing and Using Services
* List Services
  * ```php bin/console debug:container```
* List Services for logging
  * ```php bin/console debug:container log```
* Using Symfony comes down to understanding three things:
  * **_Routes and Controllers_**
  * **_Services_**
  * **_Twig Templating_**
  
### 07 Twig Templating

  
### 08 Twig Layouts

### 09 Loading CSS and JS Assets
* Copy ```Development/tutorial/web``` folders to ```symfony3-training/web```
* ```web/``` directory is web server document root and **_are publicly accessible_**
* Files outside of ```web/``` are **_not_** publicly accessible
* Use ```{{ asset('..') }}``` function load css, js and images

### 10 JSON Responses and Route Generation
* In Symfony its very easy to add an **_API endpoint_**
  * Add route and controller
  * Return JSON data in controller
  
* Added JSONView Chrome browser plugin

### 11 Generating URLs
* When creating links to other pages, instead of _hardcoding_ URLs in templates,  
  use the Twig ```path()``` function in templates
* If link URL needs to change, can just change in the controller - no need to  
  change in any templates 
  
### 12 ReactJS talks to your API
* Add ReactJS to dynamically grab and render notes

### End "Joyful Development with Symfony 3" course

#### More Symfony things to learn
* Talking to a Database
* Using Forms
* Setting up Security
* Handling API Input and Validation
* Understanding Event Listeners


### Start "Symfony 3 Fundamentals: Bundles, Configuration & Environments"

### 01 Bundles
* See ```app/AppKernel.php```
* **_A bundle is basically a Symfony plugin_** - its main job is to add   
  services to the **_service container_**
* All functionality in a Symfony application comes from **_Bundles_**
* Sample Bundles
  * ```FrameworkBundle```
  * ```TwigBundle```
  * ```DoctrineBundle```
* Add Markdown bundle (plugin)
  * Use Packagist and search for markdown
  * Follow instructions for adding knplabs/knp-markdown-bundle bundle
  
### 02 Using a Service
* Add markdown parser service to GenusController and transform text
```
$funFact = $this->get('markdown.parser')
      ->transform($funFact);
```
#### Takeaway
* **_Why add Bundles to our application ?_**
  * Because Bundles add more services to our container
  * And services are tools
  
### 03 config.yml: Control Center for Services
* Use ```app/config/config.yml``` to configure services

#### Config Commands
```
# list all bundles
./bin/console debug: config

# get config for specific bundle
./bin/console debug: config twig
```
### 04 Adding a Cache Service
* Goal: Add a cache service so we can use to avoid processing markdown 
  on each request
* Use ```DoctrineCacheBundle``` for caching
* Check via **_composer_** that ```doctrine/doctrine-cache-bundle``` is installed
* Add ```new DoctrineCacheBundle()``` to app/AppKernel.php

#### Configure cache service
* Dump configuration
```./bin/console debug:config doctrine_cache```
* Add ```doctrine_cache``` key in ```app/config/config.yml```

### 05 Configuring DoctrineCacheBundle
* Update GenusController:showAction() to use ```doctrine_cache.providers.my_markdown_cache```
* Cached output lives in ```var/cache/dev/doctrine/cache/file_system```

#### Takeaway
* **_Bundles_** give you services, and those services can be controlled in ```config.yml```
* Every **_Bundle_** works a little differently though

### 06 Environments
* In Symfony, an **_Environment_** is a set of configuration
  * database credentials
  * logging configuration
  * etc
* Symfony has two environments by default: ```dev``` and ```prod```
  * A third environment is ```test``` - used for writing automated tests
* For dev environment ```web/app_dev.php``` is run
* For prod environment ```web/app.php``` is run

### 07 config_dev.yml and config_prod.yml
* For dev environment (when ```app_dev.php``` is run), Symfony loads ```config_dev.yml```
* For prod environment (when ```app.php``` is run), Symfony loads ```config_prod.yml```
* Use FirePHP Browser extension to see log messages in Dev Tools -> Console

### 08 Caching by Environment
* Update ```app/config/config_dev.yml``` to NOT cache parsed markup output

#### Clearing Caches
```
./bin/console cache:clear --env=dev
./bin/console cache:clear --env=prod
```

### 09 Parameters - Variables of Configuration
* **_Parameters have the syntax:_** ```%parameter_name%```
* Global parameters can be set in ```app/config/parameters.yml```
* Parameters can also be set in individual ```app/config/*.yml``` files


#### Show parameters
```
bin/console debug:container --parameters
bin/console debug:container --parameter=kernel.debug
```
* Added cache_type parameter to ```app/config/config.yml``` and ```config_dev.yml```

### 10 parameters.yml and %kernel.*%
* ```app/config/parameters.yml``` holds **_machine specific_** parameters
  * Database credentials
  * Mailer config
  * etc
* ```app/config/parameters.yml```  should **_NOT_** be committed to git repo
* Parameters with ```kernel.*``` aren't defined anywhere and are automatically set by  
  Symfony
* ```app/config/parameters.yml.dist```  serves as a template for ```parameters.yml```
  and should be committed to git repo
  
### 11 Mastering Route config loading
* ```dev``` environment routes are loaded from ```app/config/routing_dev.yml```
* In all environments main routes are loaded from ```app/config/routing.yml```
* Third-party bundles can add routes
#### Show routes
```
bin/console debug:router
```
* Add homepage ```(/)``` route to app/config/routing.yml
* Add MainController.php and homepage.html.twig to render homepage
  
### End "Symfony 3 Fundamentals: Bundles, Configuration & Environments" course

### Start "Symfony 3: Doctrine and the Database" course

* See https://symfony.com/doc/3.4/doctrine.html#learn-more

### 01 Creating an Entity Class
* ```Doctrine Database Abstraction Layer```  and  
```Doctrine Object Relational Mapper (ORM)``` is used to access databases
* Using ```Doctrine ORM```
  * Each **_database table maps to a class_**
  * Each **_column in the table maps to a class property_**
* An **_Entity_** is a class that holds data 
* Doctrine maps the class to a database table

#### Using Doctrine
* Create database table by creating a class
* ```bin/console``` commands will create database and tables
* Create src/AppBundle/Entity/Genus.php
  * Use PhpStorm code generation to generate annotations

##### Reference
* https://www.doctrine-project.org/projects.html

### 02 Database Config and Automatic Table Creation
* Installed Symfony console auto-completion
  * See https://github.com/bamarni/symfony-console-autocomplete
* Specify database credentials in ```app/config/parameters.yml```
* Create file ```src/AppBundle/Entity/Genus.php```
* Create database and table(s)
```
# Create database
bin/console doctrine:database:create
Created database aqua_note for connection named default
```
```
# Show SQL command to create table
bin/console doctrine:schema:update --dump-sql
CREATE TABLE genus (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
```
```
# Create table
bin/console doctrine:schema:update --force
Updating database schema...
Database schema updated successfully! "1" query was executed
```
### 03 Insert New Objects
* Add method ```GenusController:newAction()``` and path ```genus/new ``` to insert data
* **Doctrine Entity Manager** is the _service_ that saves to and queries databases
* Run SQL query from console
```
bin/console doctrine:query:sql "select * from genus"
```

### 04 Adding More Columns
* Add remaining fields/colums to ```src/AppBundle/Entity/Genus.php```
* Update genus table to include new columns
```
bin/console doctrine:schema:update --dump-sql
ALTER TABLE genus ADD sub_family VARCHAR(255) NOT NULL, ADD species_count INT NOT NULL, ADD fun_fact VARCHAR(255) NOT NULL;
```

* Normally you would run ```bin/console doctrine:schema:update --force```  
to update database tables but in a production environment this could break  
things if for example tables are re-named or data types are modified  
* Use ```DoctrineMigrationsBundle``` instead

### 05 Database Migrations
* See https://symfony.com/doc/1.3/bundles/DoctrineMigrationsBundle/index.html
* Install the bundle
```
 composer require doctrine/doctrine-migrations-bundle
 ```
* Add bundle to AppKernel.php
* ```DoctrineMigrationsBundle``` adds a service but also add a bunch of
console commands
```
bin/console | grep migrations
  doctrine:migrations:diff                Generate a migration by comparing your current database to your mapping information.
  doctrine:migrations:execute             Execute a single migration version up or down manually.
...
```
* Migration workflow
  * Assume code was added that would affect database and/or database table(s)
  * **_Don't_** run ```bin/console doctrine:schema:update --force```
  * Run ```bin/console doctrine:migrations:diff```
    * This creates a ```Versionxxxx.php file``` in ```app/DoctrineMigrations```
  * View file and check if ok
  * Run ```bin/console doctrine:migrations:migrate``` to execute migration
  
 ### 06 Query for a List
 * Add ```GenusController:listAction()``` method to list all ```Genuses```
 
 ### 07 Entities, Twig and the Magic dot Syntax
 * Add ```app/Resources/views/genus/list.html.twig``` to display ```Genus``` list
 
### 08 Link List and Show pages also add 404 page
* Update ```GenusController:showAction()``` and and list.html.twig to show  
individual Genus content
* Add ```error404.html.twig``` custom 404 error page - this will be seen on prod  
environment though. Also added page for 500 Server errors

### 09 Fixtures: Dummy Data Rocks
* Use ```DoctrineFixturesBundle``` and ```nelmio/alice``` library
* See https://symfony.com/doc/master/bundles/DoctrineFixturesBundle/index.html
* See https://github.com/nelmio/alice
* See https://github.com/fzaninotto/Faker
```
composer require --dev doctrine/doctrine-fixtures-bundle
composer require --dev nelmio/alice:2.1.4
```
* Add file AppBundle/DataFixtures/ORM/LoadFixtures.php
* Execute load data fixtures command
```
bin/console doctrine:fixtures:load --append
```
### 10 Fixtures with Alice
* Add ```AppBundle/DataFixtures/ORM/fixtures.yml``` to use Alice

### 11 Custom Alice Faker Function
* In ```fixtures.yml``` and ```LoadFixtures.php``` add support for  
**_custom Faker formatter_** to customize Genus names
* In ```fixtures.yml``` change ```name: name<()>``` to ```name: genus<()>```

### 12 Custom Queries
* Add new genus table column isPublished (is_published)
* Update genus table with ```bin/console doctrine:migrations:migrate```
* Add ```AppBundle/Repository/GenusRepository.php``` and update 
```GenusController:listAction``` with ```findAllPublishedOrderedBySize()```

### Notes
* Use one of the following **_Repository_** methods to fetch objects
```
find()
findOneById()        // dynamic method using table column name
findOneByName()      // ditto
findAll()
findOneBy([colum1_name => value, colunn2_name => value, ...])
findBy([colum1_name => value, column2_name => value, ...])
```
* See https://symfony.com/doc/3.4/doctrine.html#doctrine-queries for more  
complex queries

### End "Symfony 3: Doctrine and the Database" course

### Start "Mastering Doctrine Relationships in Symfony 3" course

* See https://symfony.com/doc/3.4/doctrine/associations.html

### 01 Create Genus Note
* **_Goal is to replace hard-coded notes for each Genus type_**
* Create skeleton GenusNote Entity - will eventually map to genus_note table  
database
```
bin/console doctrine:generate:entity AppBundle:GenusNote
```
* Add genus_note table to aqua_note database
```
bin/console doctrine:migrations:diff
bin/console doctrine:migrations:migrate
```

* Update fixtures.yml file
* Add dummy data to genus_note table
```
bin/console doctrine:fixtures:load --append
```
### 02 ManytoOne Relationships
* Analyze relationship between tables. Is it **_ManytoOne_** or **_ManytoMany_** ?
* For genus and genus_note it's ManyToOne
* One genus can have MANY notes and many notes point to ONE genus
* Add ```genus``` property with ```ManyToOne``` **_ORM annotation_**  to ```GenusNote``` entity

### 03 Saving a Relationship
* Add genus property along with **_ManyToOne annotation_** in ```GenusNote``` class  
* In the end (after database is updated) this will have the effect of adding  
```genus_id``` and **_foreign key constraint_** in ```genus_note``` table
```
/**
   * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Genus")
   */
  private $genus;
```
* Update database
```
bin/console doctrine:migrations:diff 
bin/console doctrine:migrations:migrate 
```

### 04 Join Column and Relations in Fixtures
* By default we can save a genus note without a corresponding genus 
  * **_For integrity we should prevent this_**
* Add ```JoinColumn``` annotation to ```genus``` property in ```GenusNote```
```
@ORM\JoinColumn(nullable=false)
```
* Drop genus_note table
* Re-create genus_note table, update fixtures.yml to add genus_id when creating
a genus_note
* Load new set of genus notes each with a genus_id
```
bin/console doctrine:fixtures:load --append
```

### 05 Controller Magic: Param Conversion
* Use "Param Conversion" for ```GenusController:getNotesAction()``` method
* See https://symfony.com/doc/master/bundles/SensioFrameworkExtraBundle/annotations/converters.html


### 06 OneToMany: Inverse Side of the Relation
* In ```GenusController:getNotesAction``` method we have a genus object but how do  
we get the related notes for the object ?
* Using SQL and given we have the genus object (and id) we would do this
```
select * from genus_note where genus_id = <genus id>;
```
* Let's do it though in a more 'elegant' way
* Implement some Doctrine "sugar"
* We would like to be able to do ```$genusNote->getGenus()``` to access a ```genus``` from  
a ```genus note``` object and ```$genus->getGenusNote()``` to acess notes from a ```genus```
* Add ```$notes``` property and ```OneToMany``` annotation to Genus class
* Add ```Genus``` constructor
```
public function __construct() {
    $this->notes = new ArrayCollection();
  }
```
#### Note
* Whenever you have a relation: start by figuring out which entity should have  
the **_Foreign key_** column and then add the ```ManyToOne``` relationship there first  
This is the only side of the relationship that is _required_ 
  * It's called the **_"Owning"_** side
* Mapping the other side - the ```OneToMany``` inverse side - is always _optional_
* Update GenusController

### 07 Order By with a OneToMany
* Using ```$genus->getNotes()``` shortcut gives few query options but we can control 
the order of the notes returned
* In ```Genus``` class add ```@ORM\OrderBy({"createdAt" = "DESC"})``` annotation  
to ```$notes``` property

### 08 Tricks with ArrayCollection
* In ```GenusController:showAction``` use ```ArrayCollection``` ```filter``` function  
to get notes < 3 months old
* Loading all objects though will incur a performance hit if hundreds/thousands  
millions of records exist
* Solution is to use a custom query - See below

### 09 Querying on a Relationship
* Implement custom query ```findAllRecentNotesForGenus``` in ```GenusNoteRepository```

### 10 Query across  a JOIN
* Implement custom query ```findAllPublishedOrderedByRecentlyActive``` in ```GenusNoteRepository```



### End "Mastering Doctrine Relationships in Symfony 3" course